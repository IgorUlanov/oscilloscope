#include "Ili9320.h"
#include <stm32f10x.h>

extern const uint8_t* const *pfonts;

/* AHB clock period in nanoseconds */
#define T_HCK	14
#define LCDBUS_RSLOW_ADDR	((vu16 *)0x60000000)
#define LCDBUS_RSHIGH_ADDR	((vu16 *)(0x60000000 | (1<<(19+1))))
#define LED_LCD (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOD_BASE+0x0C-PERIPH_BASE) * 0x20) + (13*4))))
#define SPACE 12 // коэф. деления высоты символа для определения межсимвольного расстояния

void BUS_Init(void);
void setCursor(uint_fast16_t,uint_fast16_t);
void writeReg(uint_fast16_t,uint_fast16_t);
void Delay(volatile int);
void Reset_set(int);
void Reset_init(void);
void Led_init(void);
void setWindow(uint16_t x,uint16_t y,uint16_t w,uint16_t h);
void writeData(uint_fast16_t val);

//! символ растрового шрифта 16x12
/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимый символ
 * \param color - цвет символа
 * \param front - цвет фона
 */
void Symbol_16(uint16_t x, uint8_t y, uint8_t d, uint16_t color, uint16_t front){
	setWindow(x,y,x+12,y+16);
	const uint8_t *font_char =pfonts[f16]+d*24;
	for(uint8_t i=0; i<12; i++) {
		for(uint8_t j=0; j<8; ++j) {
			if(*font_char & (1u<<j)) writeData(color);
			else writeData(front);
		}
		font_char+=12;
		for(uint8_t j=0; j<8; ++j) {
			if(*font_char & (1u<<j)) writeData(color);
			else writeData(front);
		}
		font_char-=11;
	}
}

//! символ растрового шрифта 10x7
/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимый символ
 * \param color - цвет символа
 * \param front - цвет фона
 */
void Symbol_10(uint16_t x, uint8_t y, uint8_t d, uint16_t color, uint16_t front){
	setWindow(x,y,x+7,y+10);
	const uint8_t *font_char = pfonts[f10]+d*14;
	for(uint8_t i=0; i<7; i++) {
		for(uint8_t j=0; j<8; ++j) {
			if(*font_char & (1u<<j)) writeData(color);
			else writeData(front);
		}
		font_char+=7;
		for(uint8_t j=0; j<2; ++j) {
			if(*font_char & (1u<<j)) writeData(color);
			else writeData(front);
		}
		font_char-=6;
	}
}

//! символ растрового шрифта 8х6
/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимый символ
 * \param color - цвет символа
 * \param front - цвет фона
 */
void Symbol_8(uint16_t x, uint8_t y, uint8_t d, uint16_t color, uint16_t front){
	setWindow(x,y,x+6,y+8);
	const uint8_t *font_char = pfonts[0]+d*6;
	font_char+=d*6;
	for(uint8_t i=0; i<6; i++) {
		for(uint8_t j=0; j<8; ++j) {
			if(*font_char & (1u<<j)) writeData(color);
			else writeData(front);
		}
		font_char++;
	}
}

/*!
 * здесь собственно просто вызывается соответствующая функция в зависимости от шрифта в памраметрах
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимый символ
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t Symbol(uint16_t x, uint8_t y, uint8_t d, uint16_t color, uint16_t front, uint8_t font){
	void(*p)(uint16_t, uint8_t, uint8_t, uint16_t, uint16_t);
	uint16_t n=x;
	switch(font){
	case f16: p=Symbol_16; n+=12; break;
	case f10: p=Symbol_10; n+=7; break;
	default: p=Symbol_8; n+=6; break;
	}
	(*p)(x,y,d,color,front);
	return n;
}

void drawHex8(uint8_t x,uint8_t y,uint32_t c,uint16_t color, uint16_t front, uint8_t font){
	uint8_t d=(c>>28) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>24) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>20) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>16) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>12) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>8) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>4) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=c & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
}

void drawHex4(uint8_t x,uint8_t y,uint32_t c,uint16_t color, uint16_t front, uint8_t font){
	uint8_t d=(c>>12) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>8) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=(c>>4) & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
	d=c & 0x0F;
	if (d<10) d+=0x30; else d+=0x37;
	x=Symbol(x,y,d,color,front,font);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param adr - адрес первого символа строки, строка должна заканчиватся нулем.
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t Message(uint16_t x,uint16_t y, const char *adr, uint16_t color, uint16_t front, uint8_t font){
	while (*adr!=0) x=Symbol(x,y,*adr++,color,front,font);
	return x;
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимое число
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t Digital5(uint16_t x,uint16_t y,uint16_t d,uint16_t color, uint16_t front, uint8_t font){
	d=d%100000;
	x=Symbol(x,y,0x30+d/10000,color,front,font);
	d=d%10000;
	x=Symbol(x,y,0x30+d/1000,color,front,font);
	d=d%1000;
	x=Symbol(x,y,0x30+d/100,color,front,font);
	d=d%100;
	x=Symbol(x,y,0x30+d/10,color,front,font);
	d=d%10;
	return Symbol(x,y,0x30+d,color,front,font);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимое число
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t Digital3(uint16_t x,uint16_t y,uint16_t d,uint16_t color, uint16_t front, uint8_t font){
	d=d%1000;
	uint8_t k=1;
	if (d/100>0){
		x=Symbol(x,y,0x30+d/100,color,front,font);
		k=0;
	}
	d=d%100;
	if ((d/10>0) || (k==0)) x=Symbol(x,y,0x30+d/10,color,front,font);
	d=d%10;
	return Symbol(x,y,0x30+d,color,front,font);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимое число
 * \param decimal - количество знаков после десятичной точки
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t Digital(uint16_t x,uint16_t y,uint16_t d, uint8_t decimal, uint16_t color, uint16_t front, uint8_t font){
	d=d%1000;
	uint8_t k=1;
	if ((d/100>0)||(decimal==2)){
		x=Symbol(x,y,0x30+d/100,color,front,font);
		k=0;
	}
	if (decimal==2) x=Symbol(x,y,',',color,front,font);
	d=d%100;
	if ((d/10>0) || (k==0) ||(decimal==1)) x=Symbol(x,y,0x30+d/10,color,front,font);
	if (decimal==1) x=Symbol(x,y,',',color,front,font);
	d=d%10;
	return Symbol(x,y,0x30+d,color,front,font);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param d - выводимое число
 * \param decimal - количество знаков после десятичной точки
 * \param color - цвет символа
 * \param front - цвет фона
 * \param font - шрифт символа
 * \return - координата Х для следующего символа
 */
uint16_t DigitalF(uint16_t x,uint16_t y,uint16_t d, uint8_t decimal, uint16_t color, uint16_t front, uint8_t font){
	d=d%1000;
	uint8_t k=1;
	if ((d/100>0)||(decimal==2)){
		x=Symbol(x,y,0x30+d/100,color,front,font);
		k=0;
	}
	if (decimal==2) x=Symbol(x,y,',',color,front,font);
	d=d%100;
	if ((d/10>0) || (k==0) ||(decimal==1)) x=Symbol(x,y,0x30+d/10,color,front,font);
	if (decimal==1) x=Symbol(x,y,',',color,front,font);
	d=d%10;
	return Symbol(x,y,0x30+d,color,front,font);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в правом верхнем углу.
 * \param w,h - ширина и высота рамки.
 * \param back - цвет заполнения - фон.
 * \param color - цвет рамки.
 */
void setButton(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color, uint16_t back){
	Rectangle(x+1,y+1,w-1, h-1,back);
	// внешняя рамка
	Line_h(x+1,y,w-1,color);
	Line_v(x,y+1,h-1,color);
	Line_h(x+1,y+h,w-1,color);
	Line_v(x+w,y+1,h-1,color);
	// внутреняя рамка
	uint8_t r=((back>>8)+(color>>8)*2)/3;
	uint8_t g=(((back>>3)&0xFF)+((color>>3)&0xFF)*2)/3;
	uint8_t b=(((back<<3)&0xFF)+((color<<3)&0xFF)*2)/3;
	color=LCD_RGB_TO_LCDCOLOR(r,g,b);
	Line_h(x+4,y+2,w-7,color);
	Line_v(x+3,y+3,h-5,color);
	Line_h(x+4,y+h-2,w-7,color);
	Line_v(x+w-3,y+3,h-5,color);
}

/*!
 * \param color - цвет линии
 */
void Grid_h(uint16_t color){
	uint16_t y=BIAS_Y;
	while(y<216+BIAS_Y+1){
		Line_h(BIAS_X,y, 300, color);
		y+=GRID_Y;
	}
}

/*!
 * \param color - цвет линии
 */
void Grid_v(uint16_t color){
	uint16_t x=BIAS_X;
	while(x<300+BIAS_X+1){
		Line_v(x,BIAS_Y,216,color);
		x+=GRID_X;
	}
}

/*!
 * \param color - цвет заполнения
 */
void Fill(uint16_t color){
	setWindow(0,0,320,240);
	for (uint32_t i=0;i<(320*240);i++) writeData(color);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param color - цвет точки
 */
void Pixel(uint_fast16_t x, uint_fast16_t y, uint_fast16_t color){
	setCursor(x,y);
	writeData(color);// pixel
}

/*!
 * \param x - координата оси Х
 * \param y1 - начало линии по оси Y
 * \param y2 - конец линии по оси Y
 * \param color - цвет линии
 * \param front - цвет фона
 */
void clearLine(uint_fast16_t x, uint_fast8_t y1, uint_fast8_t y2, uint_fast16_t color, uint16_t front){
	if (x%GRID_X!=0){
		if (y1!=y2){
			setCursor(x+BIAS_X,y1+BIAS_Y);
			for (uint8_t i=y1;i<y2;i++){
				if (i%GRID_Y==0) writeData(front);
				else writeData(color);
			}
		}
		else if (y1%GRID_Y==0) Pixel(x+BIAS_X,y1+BIAS_Y,front); else Pixel(x+BIAS_X,y1+BIAS_Y,color);
	}
	else{
		if (y1!=y2){
			setCursor(x+BIAS_X,y1+BIAS_Y);
			for (uint8_t i=y1;i<y2;i++) writeData(front);
			}
		else Pixel(x+BIAS_X,y1+BIAS_Y,front);
	}
}

/*!
 * \param x - координата оси Х
 * \param y1 - начало линии по оси Y
 * \param y2 - конец линии по оси Y
 * \param color - цвет линии
 */
void setLine(uint_fast16_t x, uint_fast8_t y1, uint_fast8_t y2, uint_fast16_t color){
		if (y1!=y2){
			setCursor(x+BIAS_X,y1+BIAS_Y);
			for (uint8_t i=y1;i<y2;i++) writeData(color);
		}
		else Pixel(x+BIAS_X,y1+BIAS_Y,color);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param count - длина линии
 * \param color - цвет линии
 */
void Line_h(uint16_t x, uint16_t y, uint16_t count, uint16_t color){
	setWindow(x,y,x+count,y+1);
	for (uint16_t i=0;i<count;i++) writeData(color);
}

/*!
 * \param x,y - координаты по горизонтали и вертикали. Нулевые координаты в левом верхнем углу.
 * \param count - длина линии
 * \param color - цвет линии
 */
void Line_v(uint16_t x, uint16_t y, uint8_t count, uint16_t color){
	// вертикальная линия из точки ху длиной count цветом color
	setWindow(x,y,x+1,y+count);
	for (uint8_t i=0;i<count;i++) writeData(color);
}

/*!
 * \param x,y - координаты верхнего левого угла прямоугольника.
 * \param count_x - длина прямоугольника
 * \param count_y - высота прямоугольника
 * \param color - цвет линии
 */
void Rectangle(uint16_t x, uint16_t y, uint16_t count_x, uint16_t count_y, uint16_t color){
	setWindow(x,y,x+count_x,y+count_y);
	for (uint32_t i=0;i<((uint32_t)count_x*count_y);i++) writeData(color);
}

void writeReg(uint_fast16_t regn,uint_fast16_t val){
	*LCDBUS_RSLOW_ADDR = regn;
	*LCDBUS_RSHIGH_ADDR = val;
}

void setCursor(uint_fast16_t x, uint_fast16_t y){
	writeReg(0x20,y);
	writeReg(0x21,x);
	*LCDBUS_RSLOW_ADDR = 0x22;
}

void writeData(uint_fast16_t val){
	*LCDBUS_RSHIGH_ADDR = val;
}

void setWindow(uint16_t x,uint16_t y,uint16_t x1,uint16_t y1){
	// установка окна для вывода
	writeReg(0x50, y);
	writeReg(0x51, y1-1);
	writeReg(0x52, x);
	writeReg(0x53, x1-1);
	setCursor(x,y);
}

/* Initialize the FSMC bus used for driving the LCD */
void BUS_Init(void){
  FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
  FSMC_NORSRAMTimingInitTypeDef  pw,pr;

	RCC->APB2ENR |= RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOG | RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF;
	RCC->AHBENR |= RCC_AHBPeriph_FSMC;
/*-- GPIO Configuration ------------------------------------------------------*/
	// CNF записано в Begin_config
	GPIOD->CRL |= (GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE7);
	GPIOD->CRH |= (GPIO_CRH_MODE8 | GPIO_CRH_MODE9 | GPIO_CRH_MODE10 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15);
	GPIOD->CRH |= (GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13);
	GPIOE->CRL |= (GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE3 | GPIO_CRL_MODE7);
	GPIOE->CRH |= (GPIO_CRH_MODE8 | GPIO_CRH_MODE9 | GPIO_CRH_MODE10 | GPIO_CRH_MODE11);
	GPIOE->CRH |= (GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15);
	GPIOG->CRL |= (GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3);
	GPIOG->CRL |= (GPIO_CRL_MODE4 | GPIO_CRL_MODE5);

/*-- FSMC Configuration ------------------------------------------------------*/
  pr.FSMC_AddressSetupTime = (5/T_HCK+1);
  pr.FSMC_AddressHoldTime = (5/T_HCK+1);
  pr.FSMC_DataSetupTime = (100/T_HCK+1);
  pr.FSMC_BusTurnAroundDuration = 0;
  pr.FSMC_CLKDivision = 0;
  pr.FSMC_DataLatency = 0;
  pr.FSMC_AccessMode = FSMC_AccessMode_A;

  pw.FSMC_AddressSetupTime = (5/T_HCK+1);
  pw.FSMC_AddressHoldTime = (5/T_HCK+1);
  pw.FSMC_DataSetupTime = ((20+15)/T_HCK+1);
  pw.FSMC_BusTurnAroundDuration = 0;
  pw.FSMC_CLKDivision = 0;
  pw.FSMC_DataLatency = 0;
  pw.FSMC_AccessMode = FSMC_AccessMode_A;

  FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM2;
  FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
  FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
  FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
  FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
  FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
  FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
  FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
  FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Enable;
  FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Enable;
  FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &pr;
  FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &pw;
  FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);
  /* Enable FSMC Bank1_SRAM Bank */
  FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM2, ENABLE);
}

void initLCD(void){
	BUS_Init();
	Reset_init();
	Led_init();
	Reset_set(1);
	Delay(1000);
	Reset_set(0);
	Delay(1000);
	Reset_set(1);
	Delay(1000);
	Delay(500); /* delay 50 ms */
/* Start Initial Sequence ----------------------------------------------------*/
	writeReg(0x00E5,0x8000); /* Set the internal vcore voltage */
	writeReg(0x0000, 0x0001); /* Start internal OSC. */
	Delay(500);
	writeReg(0x00a4, 0x0001); /* calb */
	writeReg(0x0007, 0x0000); /* display control */
   Delay(500);
	writeReg(0x0001, 0x0100); /* SS = 1, the shift direction of outputs is from S720 to S1. */
	writeReg(0x0002, 0x0700); /* set 1 line inversion */
//	writeReg(0x0003, 0x1018); /* AM = “1”, the address is updated in vertical writing direction. I/D[1:0] = 01*/
	writeReg(0x0003, 0x1030); /* AM = “0”, the address is updated in vertical writing direction. I/D[1:0] = 11*/
	writeReg(0x0004, 0x0000); /* Resize register */
	writeReg(0x0008, 0x0202); /* set the back porch and front porch */
	writeReg(0x0009, 0x0000); /* set non-display area refresh cycle ISC[3:0] */
/* Power On sequence ---------------------------------------------------------*/
	writeReg(0x0007, 0x0101); /* power control 1 BT, AP */
	writeReg(0x0017, 0x0001); /* ????? */

	writeReg(0x0010, 0x0000); /* SAP, BT[3:0], AP, DSTB, SLP, STB */
	writeReg(0x0011, 0x0007); /* DC1[2:0], DC0[2:0], VC[2:0] */
	writeReg(0x0012, 0x0000); /* VREG1OUT voltage */
	writeReg(0x0013, 0x0000); /* VDV[4:0] for VCOM amplitude */
	Delay(2000);                 /* Dis-charge capacitor power voltage (200ms) */
	writeReg(0x0010, 0x16B0); /* SAP, BT[3:0], AP, DSTB, SLP, STB */
	writeReg(0x0011, 0x0037); /* DC1[2:0], DC0[2:0], VC[2:0] */
	Delay(500);                  /* Delay 50 ms */
	writeReg(0x0012, 0x013e); /* VREG1OUT voltage */
	Delay(500);                  /* Delay 50 ms */
	writeReg(0x0013, 0x1a00); /* VDV[4:0] for VCOM amplitude */
	writeReg(0x0029, 0x000f); /* VCM[4:0] for VCOMH */
	Delay(500);                  /* Delay 50 ms */
	writeReg(0x0020, 0x0000); /* GRAM horizontal Address */
	writeReg(0x0021, 0x0000); /* GRAM Vertical Address */

/* Set GRAM area -------------------------------------------------------------*/
	writeReg(0x0050, 0x0000); /* Horizontal GRAM Start Address */
	writeReg(0x0051, 0x00EF); /* ef Horizontal GRAM End Address */
	writeReg(0x0052, 0x0000); /* Vertical GRAM Start Address */
	writeReg(0x0053, 0x013F); /* Vertical GRAM End Address */

	writeReg(0x0060, 0x2700); /* Gate Scan Line */
//        writeReg(0x0060, 0xA700); /* Gate Scan Line */
	writeReg(0x0061, 0x0001); /* NDL,VLE, REV */
	writeReg(0x006a, 0x0000); /* set scrolling line */
/* Panel Control -------------------------------------------------------------*/
	writeReg(0x0090, 0x0010);
	writeReg(0x0092, 0x0000);
	writeReg(0x0093, 0x0000);
/* Adjust the Gamma Curve ----------------------------------------------------*/
 	writeReg(0x0030, 0x0007);
	writeReg(0x0031, 0x0403);
	writeReg(0x0032, 0x0404);
	writeReg(0x0035, 0x0002);
	writeReg(0x0036, 0x0707);
	writeReg(0x0037, 0x0606);
	writeReg(0x0038, 0x0106);
	writeReg(0x0039, 0x0007);
	writeReg(0x003c, 0x0700);
	writeReg(0x003d, 0x0707);
	writeReg(0x0007, 0x0173); /* 262K color and display ON */
	LED_LCD=0;
}

void Delay(volatile int i){
	for(;i;--i) {
		volatile int j;
		for(j=0; j<100;++j);
	}
}

void Reset_set(int val){
	if(val) {
		GPIOE->BSRR = GPIO_Pin_2;
	} else {
		GPIOE->BRR = GPIO_Pin_2;
	}
}

void Reset_init(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOE;
	GPIOE->CRL &= ~GPIO_CRL_CNF2;
	GPIOE->CRL |= GPIO_CRL_MODE2;
	Reset_set(1);
}

void Led_init(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOD;
	GPIOD->CRH &= ~GPIO_CRH_CNF13;
	GPIOD->CRH |= GPIO_CRH_MODE13;
	LED_LCD=1;
}
