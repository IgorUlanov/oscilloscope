/*
 * Touch.cpp
 *
 *  Created on: 19.07.2012
 *      Author: igor
 */

#include "touch.h"
#include <stm32f10x.h>
#include "Ili9320.h"
#include "flash.h"
#include "menu.h"

const REGION region_main[]={
		{0,53,0,70}, // - чувствительность
		{0,53,160,209},  // + чувствительность
		{70,120,210,240}, // + длительность
		{180,320,210,240}, // - длительность
		{60,320,0,180}, // выбор режима
};

#define H_ 50
#define B_ 8
#define X_ 30
const REGION region_select[]={
		{X_,320-X_,B_,H_}, // осцилограф
		{X_,320-X_,B_*2+H_,B_*2+H_*2},  // АЧХ
		{X_,320-X_,B_*3+H_*2,B_*3+H_*3}, // генератор
		{X_,320-X_,B_*4+H_*3,B_*4+H_*4}, // настройка
};

const REGION region_touch[]={
		{170,470,-100,100},
		{-150,150,-100,100},
		{-150,150,140,340},
		{170,470,140,340},
};

/// Массив структур со ссылками на массивы структур с описанием областей экрана.
const LINK_REGION link_region[]={
		{5,&region_main[0]},
		{4,&region_select[0]},
		{5,&region_main[0]},
		{4,&region_touch[0]},
};

uint8_t pressTouch(uint8_t step_menu){
	//!< возвращает номер нажатой области, или 0xFF.
	const REGION* region=link_region[step_menu].buf;
	for (uint8_t i=0;i<link_region[step_menu].count;i++){
		if ((ts_x>region->left)&&(ts_x<region->right))
			if ((ts_y>region->up)&&(ts_y<region->down)) return i;
		region++;
	}
	return 0xFF;
}

/*!
 * Область экрана ищется в массивах структур #REGION. Ссылка на необходимый массив забирается из массива
 * структур #LINK_REGION. Входной параметр является индексом в массиве #LINK_REGION.
 *
 * \param step_menu - индекс в массиве #LINK_REGION.
 * \return - номер нажатой области или 0xFF, если нажатия не было.
 */
uint8_t checkTouch(uint8_t step_menu){
	uint8_t result=0xFF;
	if (flags.pressed==0){
		if (ts_pressed>=PRESS_COUNT-1){
			result=pressTouch(step_menu);
			if (result<0xFF) flags.pressed=1;
		}
	}
	if (ts_no_pressed>=PRESS_COUNT-1){
		flags.pressed=0;
		ts_x=0; ts_y=0;
	}
	return result;
}

/// Функция используется для расчета коэффициентов сенсорного экрана по 4 крайним точкам экрана.
uint16_t calculateTouch(int32_t val_low,int32_t val_high,int32_t min,int32_t max,int32_t *a,int32_t *b){
	/*
	 * f(val) = a * val + b
	 * f(val_low) = min
	 * f(val_high) = max
	 */
	if(val_high == val_low) {
		return 1;
	}
	*a = (max - min) / (val_high - val_low);
	*b = (min * val_high - max * val_low) / (val_high - val_low);
	return 0;
}

void recalculateTouch(void){
	// пересчет коэффициентов для сенсорного дисплея
	int32_t a, b;
	if (calculateTouch((adjustment.touch.xl_yu.x + adjustment.touch.xl_yd.x)/2,
			(adjustment.touch.xr_yu.x + adjustment.touch.xr_yd.x)/2,
			0,
			240*1024,
			&a,
			&b)) return;
	 touch_coef.a_x = a;
	 touch_coef.b_x = b;
	if (calculateTouch((adjustment.touch.xl_yu.y + adjustment.touch.xr_yu.y)/2,
			(adjustment.touch.xl_yd.y + adjustment.touch.xr_yd.y)/2,
			0,
			320*1024,
			&a,
			&b)) return;
	 touch_coef.a_y = a;
	 touch_coef.b_y = b;
}

void initTouch(void){
	/*
	* Измерение резистивной матрицы тачскрина.
	* АЦП работает на частоте 12 МГц. Период измерений 252 такта = 21мкс.
	* Результаты берутся только за последние 8 измерения, в которых
	* последовательно опрашиваются каналы.
	* Оцифрованные данные скидываются в память с помощью DMA2 channel5.
	* АЦП работает в режиме scan continuous.
	*/
	// настройка портов PC0-3 (ADC123_IN10-13).

	RCC->AHBENR |= RCC_AHBPeriph_DMA2;
	RCC->APB2ENR |= RCC_APB2Periph_GPIOC | RCC_APB2Periph_ADC3;

	GPIOC->CRL &= ~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_MODE2 | GPIO_CRL_CNF2 | GPIO_CRL_MODE3 | GPIO_CRL_CNF3);
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1;

	DMA2_Channel5->CPAR=(uint32_t)&ADC3->DR;
	DMA2_Channel5->CMAR=(uint32_t)&ts_buf[0];
	DMA2_Channel5->CNDTR=16;
	DMA2_Channel5->CCR = DMA_CCR5_PL_0 | DMA_CCR5_MSIZE_0 | DMA_CCR5_PSIZE_0 | DMA_CCR5_MINC;
	DMA2_Channel5->CCR |= DMA_CCR5_CIRC | DMA_CCR5_TCIE | DMA_CCR5_EN;

	ADC3->SMPR1 = 0x00FFFFFF; // ADC_SampleTime_239Cycles5
	ADC3->SMPR2 = 0x3FFFFFFF; // ADC_SampleTime_239Cycles5
	ADC3->SQR3 = ADC_SQR3_SQ1_1 | ADC_SQR3_SQ1_3 | ADC_SQR3_SQ2_1 | ADC_SQR3_SQ2_3; // ADC_Channel_10
	ADC3->SQR3 |= ADC_SQR3_SQ3_1 | ADC_SQR3_SQ3_3 | ADC_SQR3_SQ4_1 | ADC_SQR3_SQ4_3; // ADC_Channel_10
	ADC3->SQR3 |= ADC_SQR3_SQ5_1 | ADC_SQR3_SQ5_3 | ADC_SQR3_SQ6_1 | ADC_SQR3_SQ6_3; // ADC_Channel_10
	ADC3->SQR2 = ADC_SQR2_SQ7_1 | ADC_SQR2_SQ7_3 | ADC_SQR2_SQ8_1 | ADC_SQR2_SQ8_3; // ADC_Channel_10
	ADC3->SQR2 |= ADC_SQR2_SQ9_1 | ADC_SQR2_SQ9_3 | ADC_SQR2_SQ10_1 | ADC_SQR2_SQ10_3; // ADC_Channel_10
	ADC3->SQR2 |= ADC_SQR2_SQ9_1 | ADC_SQR2_SQ9_3 | ADC_SQR2_SQ10_1 | ADC_SQR2_SQ10_3; // ADC_Channel_10
	ADC3->SQR2 |= ADC_SQR2_SQ11_0 | ADC_SQR2_SQ11_1 | ADC_SQR2_SQ11_3 | ADC_SQR2_SQ12_0 | ADC_SQR2_SQ12_1 | ADC_SQR2_SQ12_3; // ADC_Channel_11
	ADC3->SQR1 = ADC_SQR1_SQ13_2 | ADC_SQR1_SQ13_3 | ADC_SQR1_SQ14_2 | ADC_SQR1_SQ14_3; // ADC_Channel_12
	ADC3->SQR1 |= ADC_SQR1_SQ15_0 | ADC_SQR1_SQ15_2 | ADC_SQR1_SQ15_3 | ADC_SQR1_SQ16_0 | ADC_SQR1_SQ16_2 | ADC_SQR1_SQ16_3; // ADC_Channel_13
	ADC3->SQR1 |= ADC_SQR1_L;
	ADC3->CR1 = ADC_CR1_DUALMOD_1 | ADC_CR1_DUALMOD_2 | ADC_CR1_SCAN;
	ADC3->CR2 = ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL | ADC_CR2_DMA | ADC_CR2_RSTCAL | ADC_CR2_CONT | ADC_CR2_ADON ;
	while (ADC3->CR2 & ADC_CR2_RSTCAL);
	ADC3->CR2 |= ADC_CR2_CAL;
	while (ADC3->CR2 & ADC_CR2_CAL);

	EXTI->IMR |= EXTI_IMR_MR3;
	EXTI->RTSR |= EXTI_RTSR_TR3;
	EXTI->FTSR &= ~EXTI_FTSR_TR3;
	ADC3->CR2 |= ADC_CR2_SWSTART;
}
