#ifndef INIT_HPP_
#define INIT_HPP_

#ifdef __cplusplus
extern "C" {
#endif
//! начальный этап инициализации микроконтроллера
void Begin_config(void);
//! заключительный этап инициализации микроконтроллера
void Final_config(void);
//! выхода для отладки
void Led_config(void);

#ifdef __cplusplus
}
#endif
#endif /* INIT_HPP_ */
