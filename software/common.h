#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>

#define GRID_X 25
#define GRID_Y 36
#define SYMBOL LCD_RGB_TO_LCDCOLOR(0x00,0x80,00)
#define FRONT BLACK
#define GRID GRAY
#define FLASH_PAGE 2048 //!< размер страницы флеша микроконтроллера
#define TOUCH_COUNT 16 //!< момент фиксации нажатия сенсорного экрана. TOUCH_COUNT * 1.68мс.
#define PRESS_COUNT 30 // устранение дребезга 50мс
#define BIAS_X 5
#define BIAS_Y 5

#define LED1 (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOA_BASE+0x0C-PERIPH_BASE) * 0x20) + (7*4))))
#define LED2 (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOA_BASE+0x0C-PERIPH_BASE) * 0x20) + (5*4))))
#define LED3 (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOG_BASE+0x0C-PERIPH_BASE) * 0x20) + (12*4))))
#define LED4 (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOG_BASE+0x0C-PERIPH_BASE) * 0x20) + (11*4))))
#define LED5 (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOG_BASE+0x0C-PERIPH_BASE) * 0x20) + (10*4))))
#define LED1_INV (GPIOA->ODR ^= GPIO_Pin_7)
#define LED2_INV (GPIOA->ODR ^= GPIO_Pin_5)
#define LED3_INV (GPIOG->ODR ^= GPIO_Pin_12)
#define LED4_INV (GPIOG->ODR ^= GPIO_Pin_11)
#define LED5_INV (GPIOG->ODR ^= GPIO_Pin_10)

#define BLACK		0x0000
#define WHITE		0xffff
#define GRAY		0x8410
#define BLUE		0x001f
#define GREEN		0x07e0
#define RED		0xf800
#define LIGHTRED	0xfc10
#define YELLOW	0xffe0
#define MAGENTA	0xf81f
#define CYAN		0x07ff
#define GOLD	LCD_RGB_TO_LCDCOLOR(0xFF,0xD7,00)

/* Form an LCD 16-bit color out of the given 8-bit Red/Green/Blue components. */
#define LCD_RGB_TO_LCDCOLOR(R,G,B)	\
	( (((R)>>3)&0x1f)<<11 | (((G)>>2)&0x3f)<<5 | (((B)>>3)&0x1f)<<0)

//! структура с общими флагами
typedef union{
	uint32_t all;
	struct{
		uint32_t adc:1; //!< готов массив с новыми значениями координаты Y для осцилограммы.
		uint32_t dc:1; //!< если 1, то постоянное напряжение
		uint32_t amp:1; //!< необходимо перерисовать сетку напряжения.
		uint32_t storage:1; //!< необходимо перерисовать сетку длительности.
		uint32_t pressed:1; //!< после обработки нажатия снять флаг
		uint32_t oscilloscope:1; //!< флаг режима осцилограф
		uint32_t response:1; //!< флаг режима измерения АЧХ
		uint32_t start_dac:1; //!< новый запуск ГКЧ
	};
}FLAG;

extern volatile FLAG flags;

/*!
 * Структура, которая используется только внутри структуры #TS_CALIB_DATA.
 */
typedef struct {
	uint16_t x; //!< значение координаты х сенсорного экрана
	uint16_t y; //!< значение координаты у сенсорного экрана
}TS_POINT;
/*!
 * В этой структуре описаны координаты угловых точек сенсорного экрана.
 * Используется для сохранения во флеш в структуре ADJUSTMENT.
 */
typedef struct{
	TS_POINT xl_yu; //!< значение верхней правой точки сенсорного экрана
	TS_POINT xr_yu; //!< значение верхней правой точки сенсорного экрана
	TS_POINT xl_yd; //!< значение нижней левой точки сенсорного экрана
	TS_POINT xr_yd; //!< значение нижней правой точки сенсорного экрана
}TS_CALIB_DATA;
/*!
 * Структура заполняется при включении.
 * Сюда заносятся коэффициенты высчитываемые по координатом угловых точек сенсорного экрана,
 * которые хранятся в структуре TS_CALIB_DATA.
 */
typedef struct{
	int32_t a_x; //!< коэффициент координаты Х.
	int32_t b_x; //!< коэффициент координаты Х.
	int32_t a_y; //!< коэффициент координаты У.
	int32_t b_y; //!< коэффициент координаты У.
}TOUCH_C;

/*!
 * Структура которая сохраняет коэффициенты калибровки АЦП и сенсорного экрана во флеш.
 */
typedef struct{
	TS_CALIB_DATA touch; //!< координаты угловых точек сенсорного дисплея.
}ADJUSTMENT;

#endif /* COMMON_H_ */
