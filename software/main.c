/*
 * main.cpp
 *
 *  Created on: 2014
 *      Author: igor
 */

#include <stm32f10x.h>
#include "common.h"
#include "init.h"
#include "Ili9320.h"
#include "adc.h"
#include "flash.h"
#include "touch.h"
#include "menu.h"
#include "response.h"

void delay(uint32_t i);

//! структура координат дисплея для последующей очистки осцилограммы
typedef struct{
	uint8_t y1;
	uint8_t y2;
}COORD;

COORD coord[VIEW_WIDTH]; //!< массив структуры координат дисплея для последующей очистки осцилограммы

/*!
 * основная функция проекта.
 */
int main (void){
	Begin_config();
	initLCD();
	initADC();
	initDAC();
	Led_config();
	initTouch();
	loadFlash();
	recalculateTouch();
	checkMenuTouch();
	// инициализация массивов
//	for (uint16_t i=0;i<MEASURING_COUNT*2;i++) measure[i]=127;
/*	for (uint16_t i=0;i<VIEW_WIDTH;i++){
		coord[i].y1=100;
		coord[i].y2=100;
	}*/
	Final_config();
//	LED1=0;
	while(1){
/*		for (uint8_t d=0;d<8;d++){
			for (uint8_t i=0;i<8;i++){
				uint16_t n=(uint16_t)measure[i+d*8]*256+measure[i+1+d*8];
				drawHex4(i*33,d*12,n,WHITE,BLACK,f10);

			}
		}
		for (uint8_t d=0;d<8;d++){
			for (uint8_t i=0;i<8;i++){
				uint16_t n=(uint16_t)lcd[i+d*8]*256+lcd[i+1+d*8];
				drawHex4(i*33,d*12+100,n,WHITE,BLACK,f10);

			}
		}*/
//		drawHex4(150,200,lcd[60],WHITE,BLACK,f10);

/*		drawHex8(10,200,DAC->CR,WHITE,BLACK,f10);
		drawHex4(80,200,reg,WHITE,BLACK,f10);
		drawHex4(150,200,DAC->DHR12R1,WHITE,BLACK,f10);
		drawHex8(10,220,DMA2_Channel3->CMAR,WHITE,BLACK,f10);
		drawHex8(80,220,DMA2_Channel3->CCR,WHITE,BLACK,f10);
		const TABLE_SINUS* adr=ptable+6;
		Digital5(10,220,adr->repeat,WHITE,BLACK,f10);
		Digital5(80,220,adr->count,WHITE,BLACK,f10);
		drawHex8(10,200,DAC->CR,WHITE,BLACK,f10);
		drawHex8(10,220,DMA2_Channel3->CMAR,WHITE,BLACK,f10);
		drawHex8(80,220,DMA2_Channel3->CCR,WHITE,BLACK,f10);
		Digital5(150,220,DMA2_Channel3->CNDTR,WHITE,BLACK,f10);
		drawHex4(220,220,DAC->DHR12R1,WHITE,BLACK,f10);*/
//		drawHex4(33,12,GPIOF->IDR,WHITE,BLACK,f10);
		if (flags.adc==1){
			if ((getMenu()==e_refreshMain)||(getMenu()==e_refreshResponse)){
//				LED1=1;
				if (getMenu()==e_refreshResponse){
				 	lcd[0]=lcd[3];
				 	lcd[1]=lcd[3];
				 	lcd[2]=lcd[3];
				 	for (uint_fast8_t i=1;i<50;i++){
						float y=lcd[i*5-2];
						float d;
						d=(y-lcd[i*5+3])/5;
						for (uint_fast16_t n=i*5-1;n<i*5+3;n++){
							y-=d;
							lcd[n]=(uint8_t)(y+0.5);
						}
				 	}
				 	lcd[249]=lcd[248];
//				 	lcd[]=lcd[3];
//				 	lcd[]=lcd[3];
				}
				for (uint16_t i=view_start;i<view_start+view_count;i++){
					if (i<VIEW_WIDTH){
						clearLine(i,coord[i].y1,coord[i].y2,FRONT,GRAY); //18ms
						// хотя массив измеренных данных больше, осцилограмма рисуется только для VIEW_WIDTH значений.
						if (i!=0){
							if (lcd[i]>coord[i-1].y2){
								coord[i].y1=coord[i-1].y2;
								coord[i].y2=lcd[i];
							}
							else if (coord[i-1].y1>lcd[i]){
								coord[i].y2=coord[i-1].y1;
								coord[i].y1=lcd[i];
							}
							else{
								coord[i].y2=lcd[i];
								coord[i].y1=lcd[i];
							}
						}
						else{
							coord[i].y2=lcd[i];
							coord[i].y1=lcd[i];
						}
						setLine(i,coord[i].y1,coord[i].y2,YELLOW);
					}
				}
				if (getMenu()==e_refreshMain){
					if (flags.storage==1){
						flags.storage=0;
						setStorage();
					}
/*					if (flags.amp!=0){
						flags.amp=0;
						setStorage();
					}*/
				}
//				LED1=0;
			}
			flags.adc=0;
		}
	}
return (0);
}

void delay(uint32_t i){
	volatile uint32_t n=0;
	while (i>n) n++;
}
