#include "init.h"
#include <misc.h>
#include "common.h"

/*!
 * включаются все порты и настраиваются на вход с подтягивающим резистором на плюс.
 * без этого происходили сбои от наводок.
 */
void Begin_config(void){
	RCC_PCLK2Config(RCC_HCLK_Div1); // APB2 = 72MHz
	RCC_ADCCLKConfig(RCC_PCLK2_Div6); // 72/6=12MHz
	NVIC_SetVectorTable(NVIC_VectTab_FLASH,0);
	RCC->APB2ENR = RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD;
	RCC->APB2ENR |= RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF | RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO;
	// по умолчанию
	GPIOA->CRL=0x88888888;
	GPIOA->CRH=0x88888888;
	GPIOB->CRL=0x88888888;
	GPIOB->CRH=0x88888888;
	GPIOC->CRL=0x88888888;
	GPIOC->CRH=0x88888888;
	GPIOD->CRL=0x88888888;
	GPIOD->CRH=0x88888888;
	GPIOE->CRL=0x88888888;
	GPIOE->CRH=0x88888888;
	GPIOF->CRL=0x88888888;
	GPIOF->CRH=0x88888888;
	GPIOG->CRL=0x88888888;
	GPIOG->CRH=0x88888888;
	GPIOF->ODR=0xFFFFFFFF;
}

/*!
 * настройка приоритетов используемых прерываний.
 */
void Final_config(void){
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Channel3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
	NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 8;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Channel4_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 10;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 14;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void Led_config(void){
	// светодиоды
	RCC->APB2ENR |= RCC_APB2Periph_GPIOA;
	GPIOA->CRL &= ~GPIO_CRL_CNF7;
	GPIOA->CRL |= GPIO_CRL_MODE7;
}
