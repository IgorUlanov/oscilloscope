#ifndef FLASH_H_
#define FLASH_H_
#include "common.h"

extern const ADJUSTMENT flash_adjustment;
extern ADJUSTMENT adjustment;

//! функция сохранения структуры в озу во флеш.
void saveFlash(void);
//! функция копирования структуры из флеш в озу.
void loadFlash(void);

#endif /* FLASH_H_ */
