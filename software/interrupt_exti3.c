#include <stm32f10x.h>
#include "common.h"
#include "touch.h"
#include "menu.h"

volatile int16_t ts_y; //!< координата У нажима на тачскрин
volatile int16_t ts_x; //!< координата X нажима на тачскрин

TOUCH_C touch_coef;

//! Функция обработчика внешнего прерывания 3.
/*!
 * В функции происходит фильтрация значений и перевод этих значений в координаты дисплея.
 * Установка флага факта нажатия на сенсорный экран в случае, если нажатие было продолжительностью минимум
 * 1,68мс * TOUCH_COUNT = 26,88ms.
 */
void EXTI3_IRQHandler(void) {
	s32 add;
	if (ts_pressed>PRESS_COUNT-1) {
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_y[i];
		ts_x=(touch_coef.a_y*add/(16*TOUCH_COUNT)+touch_coef.b_y)/1024;
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_x[i];
		ts_y=(touch_coef.a_x*add/(16*TOUCH_COUNT)+touch_coef.b_x)/1024;
		}
	Navigator(checkTouch(getMenu()));
	EXTI->PR |= EXTI_PR_PR3;
}

