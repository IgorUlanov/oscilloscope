/*
 * Touch.h
 *
 *  Created on: 19.07.2012
 *      Author: igor
 */

#ifndef TOUCH_H_
#define TOUCH_H_
#include <stdint.h>
#include "common.h"

#define COUNT_REGION 6 // максимальное количество регионов

//! Функция возвращает номер области экрана или 0xFF, если ничего не нажато.
uint8_t checkTouch(uint8_t step_menu);
//! Функция используется для расчета коэффициентов сенсорного экрана по 4 крайним точкам экрана.
void recalculateTouch(void);
void initTouch(void);

extern uint32_t ts_buf[]; //!< массив для измеренных значений матрицы тачскрина
extern volatile int_fast8_t ts_pressed; //!< счетчик длительности нажатия тачскрина
extern volatile int_fast8_t ts_no_pressed; //!< счетчик длительности не нажатого тачскрина
extern volatile int16_t ts_y; //!< координата У нажима на тачскрин
extern volatile int16_t ts_x; //!< координата X нажима на тачскрин
extern uint_fast16_t ts_adc_y[TOUCH_COUNT]; //!< значение координаты у с АЦП
extern uint_fast16_t ts_adc_x[TOUCH_COUNT]; //!< значение координаты x с АЦП
extern TOUCH_C touch_coef;

//! cтруктура в которой содержатся координаты области нажатия сенсорного экрана.
typedef struct{
	int16_t left; //!< левая граница.
	int16_t right;	//!< правая граница.
	int16_t up; //!< верхняя граница.
	int16_t down; //!< нижняя граница.
}REGION;

//! Структура с сылками на массивы с областями сенсорного экрана.
typedef struct{
	uint8_t count;	//!< количество областей сенсорного экрана
	const REGION *buf; //!< адрес массива с регионами
}LINK_REGION;

#endif /* TOUCH_H_ */
