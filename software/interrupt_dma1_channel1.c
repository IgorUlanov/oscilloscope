#include <stm32f10x.h>
#include <common.h>
#include "adc.h"
#include "menu.h"
#include "response.h"
#include "Ili9320.h"

uint8_t measure[MEASURING_COUNT*2]; //!< массив для измеренных значений с АЦП1.

//! прерывание в котором происходит обработка измеренных значения АЦП1
/*!
 * Прерывание вызывается по заполнению половины массива #measure, каждые 12800*0.004=51.2ms. По флагу DMA_ISR_TCIF1
 * определяется какая из половин заполнена.\n
 * Предварительно расчитывается коэффициент масштаба оси Y. Он равен коэффициенту k из структуры #amplitude деленному
 * на коэффициент суммирования k структуры #storage. Расчитываются значения max и min, которые ограничивают
 * расчитанные значения Y областью осцилограммы (198 пиксель).\n
 * Дальше в цикле перебора заполненной части массива высчитывается среднеарифмитическое для каждой точки Х осцилограммы,
 * умножается на коэффициент масштаба оси Y, приводятся к допустимым границам и  сохраняются в массиве #lcd.\n
 * Поле adc флага #flags индицирует готовность массива #lcd.
 */
void DMA1_Channel1_IRQHandler(void){
	static uint32_t adc_counter; // счетчик обработанных значений
	static uint8_t freq_counter; // счетчик обработанных частот
	static uint8_t step; // счетчик прерываний в одном цикле ГКЧ

	if ((DMA1->ISR & DMA_ISR_HTIF1)||(DMA1->ISR & DMA_ISR_TCIF1)){
		uint_fast16_t counter=0;
		LED1=1;
		if (DMA1->ISR & DMA_ISR_TCIF1) counter=MEASURING_COUNT;
		if (flags.response==1){
			// режим измерения АЧХ
			if (flags.start_dac==0){
				uint32_t finish=step*MEASURING_COUNT;
				while (adc_counter<finish){
					if (freq_counter<FREQ_COUNT){
						uint16_t count=(uint16_t)(finish-adc_counter);
						if ((ptable+freq_counter)->sum<finish){
							count=(ptable+freq_counter)->sum-adc_counter;
							checkRanges(&measure[counter+adc_counter%MEASURING_COUNT],count,freq_counter);
							setResponse(freq_counter);
							freq_counter++;
							adc_counter+=2500; // 10 ms - пропуск задержки
						}
						else{
							checkRanges(&measure[counter+adc_counter%MEASURING_COUNT],count,freq_counter);
						}
						adc_counter+=count;
					}
					else{
						adc_counter=finish;
						flags.start_dac=1;	// цикл гкч закончен
					}
				}
				step++;
			}
			else{
				// запуск нового цикла ГКЧ
				flags.start_dac=0;
				TIM6->ARR=ptable->counter;
				DMA2_Channel3->CCR &= ~DMA_CCR3_EN;
				DMA2_Channel3->CMAR=(uint32_t)ptable->adr;
				DMA2_Channel3->CNDTR=ptable->length;
				dac_sinus_counter=0;
				dac_freq_counter=0;
				dac_sinus_count=ptable->repeat-1;
				DMA2_Channel3->CCR |= DMA_CCR3_EN;
				freq_counter=0;
				adc_counter=2500; // 10 ms - пропуск задержки
				for (uint16_t i=0;i<FREQ_COUNT;i++){
					margin[i].max.limit=0xFFFF;
					margin[i].max.counter=0;
					margin[i].min.limit=0;
					margin[i].min.counter=0;
				}
				step=1;
			}
		}
		if (flags.oscilloscope==1){
			// режим осцилограф
			if (flags.adc==0){
				view_start+=view_count;
				if (view_start>VIEW_WIDTH-1) view_start=0;
				uint_fast16_t count=view_start+view_count;
				if (count>VIEW_WIDTH-1) count=VIEW_WIDTH;
				for (uint_fast16_t d=view_start;d<count;d++){
					uint_fast16_t add=0;
					for (uint_fast16_t i=0;i<storage[storage_k].add;i++) add+=measure[counter++];
					counter+=storage[storage_k].k-storage[storage_k].add;
					lcd[d]=216-(uint8_t)((float)add*storage[storage_k].div);
				}
			flags.adc=1;
			}
		}
	LED1=0;
	}
	DMA1->IFCR |= DMA_IFCR_CGIF1;
}
