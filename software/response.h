#ifndef RESPONSE_H_
#define RESPONSE_H_
#include <stm32f10x.h>
#include <common.h>

#define FREQ_COUNT 50 //!< количество выводимых частот.
#define LIMIT_COUNT 4 //!< величина массива с граничными значениями

typedef struct{
	const uint16_t *adr; //!< адрес массива с синусоидой
	uint16_t length; //!< количество тактов дма за один цикл
	uint16_t repeat; //!< количество циклов дма.
	uint16_t counter; //!< делитель для таймера
	uint16_t count; //!< длительность цикла вывода частоты в мкс
	uint32_t sum; //!< суммированная длительность цикла вывода частоты в мкс
}TABLE_SINUS;

extern const TABLE_SINUS* const ptable;

typedef struct{
	uint16_t limit; //!< значение предела
	uint16_t counter; //!< количество записанных значений
	uint16_t value[LIMIT_COUNT]; //!< массив с отобранными значениями.
}LIMIT;

typedef struct{
	uint16_t freq;
	uint16_t step;
	uint32_t count;
	LIMIT max; //!< максимальные значения
	LIMIT min; //!< минимальные значения
}MARGIN;

extern MARGIN margin[];
extern const uint16_t table_bell[];
extern uint_fast16_t dac_sinus_counter;  //!< счетчик выполненых циклов ДМА
extern uint_fast16_t dac_freq_counter; //!< счетчик циклов ГКЧ
extern uint_fast16_t dac_sinus_count;  //!< количество циклов ДМА

//! сбор максимальных и минимальных значений в структуру MARGIN
void checkRanges(uint8_t *start, uint16_t count, uint8_t freq);
//!< запись в массив lcd результат обработки одной из частот
void setResponse(uint_fast8_t freq);
void initDAC(void);

#endif /* RESPONSE_H_ */
