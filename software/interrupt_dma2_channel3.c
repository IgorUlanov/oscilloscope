#include <stm32f10x.h>
#include "common.h"
#include "response.h"

uint_fast16_t dac_sinus_counter;  //!< счетчик выполненых циклов ДМА
uint_fast16_t dac_freq_counter; //!< счетчик циклов ГКЧ
uint_fast16_t dac_sinus_count;  //!< количество циклов ДМА

void DMA2_Channel3_IRQHandler(void){
	static const TABLE_SINUS* adr;
	if (DMA2->ISR & DMA_ISR_TCIF3){
		dac_sinus_counter++;
		if (dac_sinus_counter>dac_sinus_count){
			// закончились циклы одной частоты
			dac_freq_counter++;
			if (dac_freq_counter<FREQ_COUNT){
				// следующая частота
				adr=ptable+dac_freq_counter;
				TIM6->ARR=adr->counter;
				DMA2_Channel3->CCR &= ~DMA_CCR3_EN;
				DMA2_Channel3->CMAR=(uint32_t)adr->adr;
				DMA2_Channel3->CNDTR=adr->length;
				DMA2_Channel3->CCR |= DMA_CCR3_EN;
				dac_sinus_counter=0;
				dac_sinus_count=adr->repeat-1;
			}
		}
		else{
			DMA2_Channel3->CCR &= ~DMA_CCR3_EN;
			DMA2_Channel3->CMAR=(uint32_t)adr->adr;
			DMA2_Channel3->CNDTR=adr->length;
			DMA2_Channel3->CCR |= DMA_CCR3_EN;
		}
	}
	DMA2->IFCR |= DMA_IFCR_CGIF3;
}
