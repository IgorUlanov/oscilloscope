#include <stm32f10x.h>
#include "common.h"

volatile int_fast8_t ts_pressed; //!< счетчик длительности нажатия тачскрина
volatile int_fast8_t ts_no_pressed; //!< счетчик длительности не нажатого тачскрина
uint_fast16_t ts_adc_y[TOUCH_COUNT]; //!< значение координаты у с АЦП
uint_fast16_t ts_adc_x[TOUCH_COUNT]; //!< значение координаты x с АЦП
uint16_t ts_buf[16]; //!< массив для измеренных значений матрицы тачскрина

/*!
 * Функция обработчика прерывания канала 5 ПДП 2.
 * Функция вызывается каждые 336мкс, так как период измерения АЦП 3 - 252 тактов, а размер цикла ПДП 16 измерений.
 * 0.083мкс * 252 = 21 мкс * 16 = 336 мкс.
 * Здесь происходит предварительный разбор результатов измерения резистивной матрицы сенсорного экрана и концевиков
 * приспособления. Окончательная обработка происходит в функции #EXTI3_IRQHandler, которая вызывается каждый пятый
 * вызов функции = 336мкс * 5 = 1.68 мс.
 */
void DMA2_Channel4_5_IRQHandler(void)
{
	/*
	 * YD	Pin_0
	 * YU Pin_1
	 * XL	Pin_2
	 * XR Pin_3
	 * GPIOC
	 *  ts_buf[8-9] YD
	 *  ts_buf[10-11] YU
	 *  ts_buf[12-13] XL
	 *  ts_buf[14-15] XR
	 */
  static int step;
  static int ts_step;
  static int val;
  int x;
  if ((DMA2->ISR & DMA_ISR_TCIF5)!=0) {
  	switch (step){
  		case 0:
  			// результат измерения XL XR
  			GPIOC->BSRR=GPIO_BSRR_BR1|GPIO_BSRR_BS0; // YU=0, YD=1
  			val=ts_buf[12]+ts_buf[13]+ts_buf[14]+ts_buf[15];
  			break;
  		case 1:
  			// результат измерения XL XR
  			GPIOC->CRL &=~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1);
  			GPIOC->CRL |= (GPIO_CRL_MODE2 | GPIO_CRL_MODE3);
  			GPIOC->BSRR=GPIO_BSRR_BR2|GPIO_BSRR_BS3; // XL=0, XR=1
  			x=ts_buf[12]+ts_buf[13]+ts_buf[14]+ts_buf[15];
  			ts_adc_y[ts_step]=val+(4096*4-x);
  			break;
  		case 2:
  			// результат измерения YU YD
  			GPIOC->BSRR=GPIO_BSRR_BS2|GPIO_BSRR_BR3; // XL=1, XR=0
  			val=ts_buf[8]+ts_buf[9]+ts_buf[10]+ts_buf[11];
  			break;
  		case 3:
  			// результат измерения YU YD
  			GPIOC->CRL &=~(GPIO_CRL_MODE2 | GPIO_CRL_MODE3);
  			GPIOC->CRL |= (GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_CNF2_1 | GPIO_CRL_CNF3_1);
  			GPIOC->BSRR= (GPIO_BSRR_BR1 | GPIO_BSRR_BR0 | GPIO_BSRR_BS2 | GPIO_BSRR_BS3); // YU=0, YD=0
  			x=ts_buf[8]+ts_buf[9]+ts_buf[10]+ts_buf[11];
  			ts_adc_x[ts_step++]=val+(4096*4-x);
  			break;
  		case 4:
  			//  нажат ли тачскрин?
  			if (GPIOC->IDR & GPIO_IDR_IDR2) { // тачскрин не нажат
  				if (ts_no_pressed<PRESS_COUNT) ts_no_pressed++;
  				if (ts_pressed>0) ts_pressed--;
  			}
  			else {
  				if (ts_pressed<PRESS_COUNT) ts_pressed++;
  				if (ts_no_pressed>0) ts_no_pressed--;
  			}
  			GPIOC->CRL &=~(GPIO_CRL_MODE2 | GPIO_CRL_MODE3 | GPIO_CRL_CNF2_1 | GPIO_CRL_CNF3_1);
  			GPIOC->CRL |= (GPIO_CRL_MODE0 | GPIO_CRL_MODE1);
  			GPIOC->BSRR= (GPIO_BSRR_BS1 | GPIO_BSRR_BR0); // YU=1, YD=0
  			break;
  	}
  	step++;
  	if (step>4) {
  		step=0;
  		if (ts_step>TOUCH_COUNT-1) {
  			// 336*5=1,68*TOUCH_COUNT=26,88ms.
  			ts_step=0;
  			EXTI->SWIER |= EXTI_SWIER_SWIER3; // вызов прерывания для фильтрации значений
  			}
  		}
   }
  DMA2->IFCR |= DMA_IFCR_CGIF5;
}
