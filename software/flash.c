#include "flash.h"
#include <stm32f10x_flash.h>

const uint16_t* flash; //!< адрес во флеше
uint16_t* ram; //!< адрес в озу

//! копия в озу структуры #flash_adjustment с коэффициентами калибровки.
ADJUSTMENT adjustment;
/*!
 * структура во флеш со значениями поправочных коэффициентов для сенсорного дисплея.
 */
const ADJUSTMENT flash_adjustment __attribute__ ((section (".settings_ld"))) = {
		{{0x00f6,0x0748},
		{0x0709,0x0745},
		{0x00e2,0x00da},
		{0x06fa,0x00ce}}
};
/*!
 * Функция осуществляет поиск чистой области в странице флеш. Чистой область считается, если первое слово не равно 0xFFFF.
 * Адрес найденной области записывается в переменную #flash.
 */
void searchFlash(void){
	flash=(const uint16_t*)&flash_adjustment;
	ram=(uint16_t*)&adjustment;
	for (uint_fast16_t i=0;i<FLASH_PAGE/(sizeof(ADJUSTMENT));i++){
		if (*flash==0xFFFF) break;
		flash+=sizeof(ADJUSTMENT)/2;
		}
}
/*!
 * Функция считывает структуру #ADJUSTMENT из флеш в озу в переменную #adjustment.
 */
void loadFlash(void){
	searchFlash();
	flash-=sizeof(ADJUSTMENT)/2;
	for (uint16_t i=0;i<FLASH_PAGE/(sizeof(ADJUSTMENT));i++) *ram++=*flash++;
}

/*!
 * Функция записывает во флеш структуру #ADJUSTMENT из озу из переменной #adjustment.
 * Если страница заполнена, то страница стирается и структура записывается в начальную область страницы.
 */
void saveFlash(void){
	searchFlash();
	if (((flash+(sizeof(ADJUSTMENT)/2))-(const uint16_t*)&flash_adjustment)>=FLASH_PAGE/2){
		flash=(const uint16_t*)&flash_adjustment;
		FLASH_Unlock();
		FLASH_ErasePage((uint32_t)flash);
		}
	else FLASH_Unlock();
	for (uint_fast16_t i=0;i<(sizeof(ADJUSTMENT)/2);i++){
		FLASH_ProgramHalfWord((uint32_t)flash++,*ram++);
		}
	FLASH_Lock();
}

