#include "menu.h"
#include "touch.h"
#include "Ili9320.h"
#include "flash.h"
#include "adc.h"

uint8_t storage_k; //!< значение положения делителя горизонтальной развертки
uint8_t storage_k_new; //!< новое значение положения делителя горизонтальной развертки
uint8_t amplitude_k; //!< значение положения делителя вертикальной развертки
uint8_t number; //!< номер актуального меню
volatile FLAG flags; //!< структура с общими флагами

void refreshMenuTouch(uint_fast8_t);
void refreshMain(uint_fast8_t);
void initMain(uint_fast8_t);
void initSelect(uint_fast8_t);
void refreshSelect(uint_fast8_t);
void initResponse(uint_fast8_t);
void refreshResponse(uint_fast8_t);
void setFreq(void);


uint8_t getMenu(void){
	return number;
}

/*!
 * простой переключатель на функции в зависимости от номера меню в переменной #number
 */
void Navigator(uint_fast8_t key){
	switch(number){
	case e_refreshMain: refreshMain(key); break;
	case e_refreshSelect: refreshSelect(key); break;
	case e_refreshResponse: refreshResponse(key); break;
	case e_refreshMenuTouch: refreshMenuTouch(key);break;
	case e_initMain: initMain(key); break;
	case e_initSelect: initSelect(key); break;
	case e_initResponse: initResponse(key); break;
	}
}
/*!
 * очистка дисплея, инициализация переменных, прорисовка сетки.
 */
void initMenu(uint_fast8_t key){
	(void)key;
	Fill(FRONT);
	storage_k=5;
	storage_k_new=storage_k;
	amplitude_k=4;
	Grid_v(GRAY);
	Grid_h(GRAY);
	setStorage();
	number=e_refreshMain;
	flags.oscilloscope=1;
}

void initMain(uint_fast8_t key){
	(void)key;
	Fill(FRONT);
	Grid_v(GRAY);
	Grid_h(GRAY);
	setStorage();
	number=e_refreshMain;
	flags.oscilloscope=1;
}

/*!
 * обработка нажатий на сенсорный дисплей в режиме осцилоскопа.
 * Здесь доступно управление масштабом осей Х (длительность) и Y (чувствительность).
 */
void refreshMain(uint_fast8_t key){
	switch(key){
	case 0:
		// - чувствительность
		//if (amplitude_k>0) {amplitude_k--; flags.amp=1;}
		break;
	case 1:
		// + чувствительность
//		if (amplitude_k<AMPLITUDE_K) {amplitude_k++; flags.amp=1;}
		break;
	case 2:
		// + длительность
//		if (storage_k_new<STORAGE_K-1) {storage_k_new++; flags.storage=1;} break;
	case 3:
		// - длительность
//		if (storage_k_new>0) {storage_k_new--; flags.storage=1;} break;
	case 4:
		// выбор режима
		number=e_initSelect;
		flags.oscilloscope=0;
		break;
	default:
		switch(GPIOF->IDR){
		case 0xFFBF: if (storage_k_new!=0) {storage_k_new=0; flags.storage=1;} break;
		case 0xBFFF: if (storage_k_new!=1) {storage_k_new=1; flags.storage=1;} break;
		case 0xFBFF: if (storage_k_new!=2) {storage_k_new=2; flags.storage=1;} break;
		case 0xFF7F: if (storage_k_new!=3) {storage_k_new=3; flags.storage=1;} break;
		case 0xFEFF: if (storage_k_new!=4) {storage_k_new=4; flags.storage=1;} break;
		case 0xEFFF: if (storage_k_new!=5) {storage_k_new=5; flags.storage=1;} break;
		case 0xDFFF: if (storage_k_new!=6) {storage_k_new=6; flags.storage=1;} break;
		case 0x7FFF: if (storage_k_new!=7) {storage_k_new=7; flags.storage=1;} break;
		case 0xF7FF: if (storage_k_new!=8) {storage_k_new=8; flags.storage=1;} break;
		case 0xFDFF: if (storage_k_new!=9) {storage_k_new=9; flags.storage=1;} break;
		}
		if (GPIOB->IDR & 0x0080){
			if (flags.dc==0){
				flags.dc=1;
				flags.storage=1;
				DAC->DHR12R2=0;
			}
		}
		 else{
				if (flags.dc==1){
					flags.dc=0;
					flags.storage=1;
					DAC->DHR12R2=2047;
				}
		 }
		break;
	}
}

void initSelect(uint_fast8_t key){
	const char message[][14]={
			"ОСЦИЛОГРАФ",
			"измерение АЧХ",
			"ГЕНЕРАТОР",
			"НАСТРОЙКА"
	};
	const uint8_t m[]={100,80,106,106,};
	(void)key;
	if (flags.adc==0){
		uint8_t y=8;
		uint8_t b=8;
		uint8_t h=50;
		uint16_t x=30;
		for (uint8_t i=0;i<4;i++){
			setButton(x,y,320-2*x,h,GOLD,BLACK);
			Message(m[i],y+20,&message[i][0],SYMBOL,FRONT,f16);
			y+=h+b;
		}
		number=e_refreshSelect;
	}
}

void refreshSelect(uint_fast8_t key){
	switch(key){
	case 0:
		// осцилограф
		number=e_initMain; break;
	case 1:
		// измерение АЧХ
		number=e_initResponse; break;
	case 2:
		// + длительность
//		if (storage_k_new<STORAGE_K) {storage_k_new++; flags.storage=1;} break;
	case 3:
		// - длительность
//		if (storage_k_new>0) {storage_k_new--; flags.storage=1;} break;
	case 4:
		// выбор режима
		number=e_initSelect;
		break;
	}
}

void initResponse(uint_fast8_t key){
	(void)key;
	for (uint16_t i=0;i<VIEW_WIDTH;i++) lcd[i]=200;
	Fill(FRONT);
	Grid_v(GRAY);
	Grid_h(GRAY);
	setFreq();
	setBell();
	flags.start_dac=1;
	flags.response=1;
	view_count=VIEW_WIDTH;
	view_start=0;
	number=e_refreshResponse;
}

void refreshResponse(uint_fast8_t key){
	switch(key){
	case 0:
		// - чувствительность
		break;
	case 1:
		// + чувствительность
		break;
	case 2:
		// + длительность
		break;
	case 3:
		// - длительность
		break;
	case 4:
		// выбор режима
		DAC->DHR12R2=2047;
		flags.response=0;
		number=e_initSelect;
		break;
	}
}

/*!
 * инициализация режима калибровки сенсорного дисплея.
 */
void initMenuTouch(void){
	const char message_touch[][26]={
			"  В каждом углу последо- \0",
			"вательно будут зажигаться\0",
			"точки.\0",
			"  Нажимайте на них.\0",
			"  Для более точной калиб-\0",
			"ровки лучше нажимать,\0",
			"например, ручкой.\0"
	};
	Rectangle(0,0,320,240,BLACK);
	for (uint16_t i=0;i<7;i++) Message(4,i*24+20,&message_touch[i][0],YELLOW,BLACK,f16);
	Rectangle(1,1,4,4,WHITE);
	number=e_refreshMenuTouch;
}

/*!
 * обработка нажатий на сенсорный дисплей в режиме калибровки сенсорного дисплея.
 * Ожидает последовательного нажатия по 4 углам дисплея, начиная с верхнего левого по часовой стрелке.
 */
void refreshMenuTouch(uint_fast8_t key){
	uint32_t add;
	static TS_CALIB_DATA touch;
	switch(key){
	case 0:
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_y[i];
		touch.xl_yd.y=add/(16*TOUCH_COUNT);
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_x[i];
		touch.xl_yd.x=add/(16*TOUCH_COUNT);
		Rectangle(1,1,4,4,LCD_RGB_TO_LCDCOLOR(33,93,33));
		// правая верхняя точка
		Rectangle(315,1,4,4,WHITE);
		break;
	case 1:
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_y[i];
		touch.xl_yu.y=add/(16*TOUCH_COUNT);
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_x[i];
		touch.xl_yu.x=add/(16*TOUCH_COUNT);
		Rectangle(315,1,4,4,LCD_RGB_TO_LCDCOLOR(33,93,33));
		// правая нижняя точка
		Rectangle(315,235,4,4,WHITE);
		break;
	case 2:
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_y[i];
		touch.xr_yu.y=add/(16*TOUCH_COUNT);
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_x[i];
		touch.xr_yu.x=add/(16*TOUCH_COUNT);
		Rectangle(315,235,4,4,LCD_RGB_TO_LCDCOLOR(33,93,33));
		// левая нижняя точка
		Rectangle(1,235,4,4,WHITE);
		break;
	case 3:
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_y[i];
		touch.xr_yd.y=add/(16*TOUCH_COUNT);
		add=0;
		for (int i=0;i<TOUCH_COUNT;i++) add+=ts_adc_x[i];
		touch.xr_yd.x=add/(16*TOUCH_COUNT);
		adjustment.touch=touch;
		recalculateTouch();
		saveFlash();
		initMenu(0);
		break;
	}
}

/*!
 * функция сравнивает структуру #adjustment со значениями по умолчанию, если все поля совпадают,
 * значит калибровка не производилась и будет запущен режим калибровки.
 */
void checkMenuTouch(void){
	const TS_CALIB_DATA calib=
			{{0x00f6,0x0748},
			{0x0709,0x0745},
			{0x00e2,0x00da},
			{0x06fa,0x00ce}};
	uint8_t c=0;
	if (calib.xl_yu.x!=adjustment.touch.xl_yu.x) c+=1;
	if (calib.xl_yu.y!=adjustment.touch.xl_yu.y) c+=1;
	if (calib.xr_yu.x!=adjustment.touch.xr_yu.x) c+=1;
	if (calib.xr_yu.y!=adjustment.touch.xr_yu.y) c+=1;
	if (calib.xl_yd.x!=adjustment.touch.xl_yd.x) c+=1;
	if (calib.xl_yd.y!=adjustment.touch.xl_yd.y) c+=1;
	if (calib.xr_yd.x!=adjustment.touch.xr_yd.x) c+=1;
	if (calib.xr_yd.y!=adjustment.touch.xr_yd.y) c+=1;
//	if (c==0) initMenuTouch(); // необходима калибровка
//	else initMenu(0);
	initMenu(0); // при отладке раcкоментировать эту строку и закоментировать 2 предыдущих.
}

/*!
 * в зависимости от масштаба оси Х (переменная #storage_k) из массива структур #storage выбирается значение
 * кратности сетки и символ размерности, с помощью которых производится оцифровка оси X.
 */
void setStorage(void){
	storage_k=storage_k_new;
	uint16_t y=225;
	uint16_t x=10;
	x=Digital(x,y,storage[storage_k].step,storage[storage_k].decimal,SYMBOL, FRONT, f10);
	x=Message(x,y,storage[storage_k].r,SYMBOL, FRONT, f10);
	if (flags.dc==1) x=Message(x,y,"  DC ",SYMBOL,FRONT,f10);
	else  x=Message(x,y,"  AC ",SYMBOL,FRONT,f10);
//	x=Digital(x,y,amplitude[amplitude_k].step,amplitude[amplitude_k].decimal,SYMBOL, FRONT, f10);
//	x=Message(x,y,amplitude[amplitude_k].r,SYMBOL, FRONT, f10);
	if (storage[storage_k].k>STORAGE_MAX) view_count=(uint16_t)(MEASURING_COUNT/storage[storage_k].k);
	else view_count=VIEW_WIDTH;
	view_start=0;
//	flags.storage=1;
}

/*!
 * в зависимости от масштаба оси Y (переменная #amplitude_k) из массива структур #amplitude выбирается значение
 * кратности сетки и символ размерности, с помощью которых производится оцифровка оси Y.
 */
void setBell(void){
	uint16_t x=258;
	Rectangle(x, 0, 50, 220, FRONT);
	uint16_t t=Digital(x,1,0,0,SYMBOL, FRONT, f10);
	Message(t,1,"db",SYMBOL, FRONT, f10);
	t=Digital(x,51,12,0,SYMBOL, FRONT, f10);
	Message(t,51,"db",SYMBOL, FRONT, f10);
	t=Digital(x,101,24,0,SYMBOL, FRONT, f10);
	Message(t,101,"db",SYMBOL, FRONT, f10);
	t=Digital(x,151,36,0,SYMBOL, FRONT, f10);
	Message(t,151,"db",SYMBOL, FRONT, f10);
	t=Digital(x,201,48,0,SYMBOL, FRONT, f10);
	Message(t,201,"db",SYMBOL, FRONT, f10);
}

void setFreq(void){
	uint16_t y=211;
	Rectangle(20, y, 200, 20, FRONT);
	uint16_t t=Digital(20,y,75,0,SYMBOL, FRONT, f10);
	Message(t,y,"Hz",SYMBOL, FRONT, f10);
	t=Digital(65,y,250,0,SYMBOL, FRONT, f10);
	Message(t,y,"Hz",SYMBOL, FRONT, f10);
	t=Digital(115,y,9,1,SYMBOL, FRONT, f10);
	Message(t,y,"kHz",SYMBOL, FRONT, f10);
	t=Digital(170,y,3,0,SYMBOL, FRONT, f10);
	Message(t,y,"kHz",SYMBOL, FRONT, f10);
	t=Digital(215,y,10,0,SYMBOL, FRONT, f10);
	Message(t,y,"kHz",SYMBOL, FRONT, f10);
}
